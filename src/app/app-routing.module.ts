import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import {LoginComponent} from './components/login/login.component';
import { SearchComponent } from './components/search/search.component';
import {SignUpComponent} from './components/sign-up/sign-up.component';
import {CompanyComponent} from './components/company/company.component';
import {SignUpCompanyComponent} from './components/sign-up-company/sign-up-company.component';
import {JobViewComponent} from './components/job-view/job-view.component';
import { MyJobsComponent } from './components/my-jobs/my-jobs.component';
import { AddJobComponent } from './components/add-job/add-job.component';


const routes: Routes = [
{
  path:'login',
  component: LoginComponent
},
{
  path:'search',
  component: SearchComponent
},
{
  path:'sign-up',
  component: SignUpComponent
},
{
  path:'company-home',
  component: CompanyComponent
},
{
  path:'sign-up-company',
  component:SignUpCompanyComponent
},
{
  path:'job-view',
  component:JobViewComponent
},
{
  path:'my-jobs',
  component: MyJobsComponent
},
{
  path:'add-job',
  component: AddJobComponent
}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
