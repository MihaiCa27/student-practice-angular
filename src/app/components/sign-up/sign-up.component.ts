import { Component, OnInit } from '@angular/core';
import { RouterModule, Router } from '@angular/router';
import { HttpClient,HttpHeaders,HttpParams,HttpErrorResponse } from '@angular/common/http';
import {Observable, throwError} from 'rxjs';
import {catchError, tap, map} from 'rxjs/operators';

import {IUser} from  '../../interfaces/IUser';
import { SignUpService} from '../../services/sign-up.service';

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.css']
})
export class SignUpComponent implements OnInit {
   constructor(private router: Router,private signUpService: SignUpService) { }

    ngOnInit() {
    }
    errorMessage: string='';
    _password: string='';
    _firstName:string='';
    _lastName:string='';
    _email:string='';
    validEmail: string='0';
    user: IUser;

    get email(): string {
      return this._email;
    }

    set email(value:string) {
      this._email = value;
    }

    get password(): string {
      return this._password;
    }

    set password(value:string) {
      this._password = value;
    }

    get firstName(): string {
      return this._firstName;
    }

    set firstName(value:string) {
      this._firstName = value;
    }

    get lastName(): string {
      return this._lastName;
    }

    set lastName(value:string) {
      this._lastName = value;
    }

    register(email:string, firstName:string, lastName:string){
      console.log("Register");


      if(this._firstName==='' || this._email==='' || this._lastName==='' || this._password===''){
        this.errorMessage='Fill the mandatory fields';
      }
      else{
          this.errorMessage='';

          console.log(this._firstName,this._lastName,this._email,this._password);
          // this.signUpService.existsClientWithEmail(email).subscribe(
          //   data=>{
          //     if(data===null){
                this.signUpService.saveClient(this._email,this._firstName,this._lastName,this._password).subscribe( x => console.log("SUBSCRIBE"));
                // window.location.reload();
              // }
              // else{
              //   this.errorMessage='This email already exists!';
              // }
            }
          // );
          this.login();
      }


    login(){
      this.router.navigate(['/login']);
    }
}
