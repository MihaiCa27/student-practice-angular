import { Component, OnInit } from '@angular/core';
import { RouterModule, Router } from '@angular/router';
import { HttpClient,HttpHeaders,HttpParams,HttpErrorResponse } from '@angular/common/http';
import {Observable, throwError} from 'rxjs';
import {catchError, tap, map} from 'rxjs/operators';
import { Location} from "@angular/common";

import {IJobs} from '../../interfaces/IJobs';
import {AuthService} from '../../services/auth.service';
import {CompanyJobsService} from '../../services/company-jobs.service';


@Component({
  selector: 'app-company',
  templateUrl: './company.component.html',
  styleUrls: ['./company.component.css']
})
export class CompanyComponent implements OnInit {
  jobs : IJobs[];
  jsonObj: string='';
filterByCategory: string ='';
selectedJobs : IJobs;
errorMessage: string;
filteredJobs: IJobs[];
_listFilter: string;
userFilter: string;


get listFilter(): string {
    return this._listFilter;
  }

  set listFilter(value:string) {
    this._listFilter = value;
    this.filteredJobs = this._listFilter ? this.performFilter(this._listFilter) : this.jobs;
    console.log(this.filteredJobs);
  }


  constructor(private companyJobsService: CompanyJobsService, auth:AuthService, private router: Router, private location: Location) {
    // this.userFilter = auth.getEmail();
   }

  ngOnInit() {
    console.log("Getting jobs");
    console.log(sessionStorage.getItem('token'));
    this.companyJobsService.getJobs().subscribe(
      jobs => {
        this.jobs = jobs;
        this.filteredJobs = this.jobs;// this.performFilter(JSON.parse(sessionStorage.getItem('currentUser')).lastName);//this.jobs;
      },
      error => this.errorMessage = <any>error
    );

  }

performFilter(filterByCompany: string): IJobs[] {
  console.log("Name: " + JSON.parse(sessionStorage.getItem('currentUser')).lastName);
  filterByCompany = filterByCompany.toLocaleLowerCase();
  console.log("JOBS: " + this.jobs);
  return  this.jobs.filter((job: IJobs) =>console.log(job.company.name));
  //job.company.name.toLocaleLowerCase().indexOf(filterByCompany) !== -1 );
}

withdrawJob(job:IJobs){
  console.log(job._id);
  this.companyJobsService.withdrawJob(job._id).subscribe(
    c => {this.reload();}
  );

}

reload(){
  location.reload();
}

}
