import { Component, OnInit } from '@angular/core';
import { RouterModule, Router } from '@angular/router';
import { HttpClient,HttpHeaders,HttpParams,HttpErrorResponse } from '@angular/common/http';
import {Observable, throwError} from 'rxjs';
import {catchError, tap, map} from 'rxjs/operators'

import {IUser} from  '../../interfaces/IUser';
import { SignUpCompanyService} from '../../services/sign-up-company.service';

@Component({
  selector: 'app-sign-up-company',
  templateUrl: './sign-up-company.component.html',
  styleUrls: ['./sign-up-company.component.css']
})
export class SignUpCompanyComponent implements OnInit {

  constructor(private router: Router,private signUpCompanyService: SignUpCompanyService) { }

  ngOnInit() {
  }
  errorMessage: string;
  email: string;
  password: string;
  name:string;
  description:string;
  phoneNumber:string;
  location:string;
  noOfEmployees: number;
  validEmail: string='0';
  user: IUser;

  get _email(): string {
    return this.email;
  }

  set _email(value:string) {
    this.email = value;
  }

  get _password(): string {
    return this.password;
  }

  set _password(value:string) {
    this.password = value;
  }

  get _name(): string {
    return this.name;
  }

  set _name(value:string) {
    this.name = value;
  }

  get _description(): string {
    return this.description;
  }

  set _description(value:string) {
    this.description = value;
  }

  get _phoneNumber(): string {
    return this.phoneNumber;
  }

  set _phoneNumber(value:string) {
    this.phoneNumber = value;
  }

  get _location(): string {
    return this.location;
  }

  set _location(value:string) {
    this.location = value;
  }

  get _noOfEmployees(): number {
    return this.noOfEmployees;
  }

  set _noOfEmployees(value:number) {
    this.noOfEmployees = value;
  }

  register(email:string, password:string, name:string, description:string, phoneNumber: string, location: string, noOfEmployees: number){
    console.log("Register company");


    if(this.email==='' || this.password==='' || this.name===''|| this.phoneNumber===''){
      this.errorMessage='Fill the mandatory fields';
    }
    else{
        this.errorMessage='';

        console.log(this.email,this.password,this.name,this.description, this.phoneNumber, this.location, this.noOfEmployees);
        // this.signUpService.existsClientWithEmail(email).subscribe(
        //   data=>{
        //     if(data===null){

              this.signUpCompanyService.saveClient(this.name,this.description,this.location,this.phoneNumber, this.email, this.password, this.noOfEmployees).subscribe();
              // window.location.reload();
            // }
            // else{
            //   this.errorMessage='This email already exists!';
            // }
          }
        // );
        this.login();
    }


  login(){
    this.router.navigate(['/login']);
  }
}
