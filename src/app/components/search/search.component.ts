import { Component, OnInit } from '@angular/core';
import { RouterModule, Router } from '@angular/router';
import { HttpClient,HttpHeaders,HttpParams,HttpErrorResponse } from '@angular/common/http';
import {Observable, throwError} from 'rxjs';
import {catchError, tap, map} from 'rxjs/operators';
import { Location} from "@angular/common";

import {IJobs} from '../../interfaces/IJobs';
import {AuthService} from '../../services/auth.service';
import {JobService} from '../../services/job.service';
import {JobViewService} from '../../services/job-view.service';


@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {

  jobs : IJobs[];
  jsonObj: string='';
  jsonCategory:string='';
filterByCategory: string ='';
selectedJob : IJobs;
errorMessage: string;
filteredJobs: IJobs[];
_listFilter: string;
userFilter: string;


get listFilter(): string {
    return this._listFilter;
  }

  set listFilter(value:string) {
    this._listFilter = value;
    this.filteredJobs = this._listFilter ? this.performFilter(this._listFilter) : this.jobs;
    console.log(this.filteredJobs);
  }


  constructor(private jobService: JobService, private auth:AuthService, private router: Router, private location: Location, private jobViewService: JobViewService) {
    // this.userFilter = auth.getEmail();
   }

  ngOnInit() {
    this.jobService.getJobs().subscribe(
      jobs => {
        this.jobs = jobs;
        this.filteredJobs = this.jobs;
      },
      error => this.errorMessage = <any>error
    );
  }

performFilter(filterByTitle: string): IJobs[] {
  filterByTitle = filterByTitle.toLocaleLowerCase();
  return  this.jobs.filter((job: IJobs) =>
  job.title.toLocaleLowerCase().indexOf(filterByTitle) !== -1 );
}

filterByIT(){
  this.filteredJobs = this.performFilter_byCategory_IT();
}

performFilter_byCategory_IT():IJobs[]{
  //TODO
    return  this.jobs.filter((job: IJobs) => {
    this.jsonObj = JSON.stringify(job);
    this.jsonCategory = JSON.parse(this.jsonObj).category;

    console.log("LEEEEEL " + this.jsonObj);
    console.log("LEEEEEL CATEGORY " + this.jsonCategory);
    console.log("JOB CATEGORY "+job.category);
    job.category.indexOf(this.jsonCategory) !== -1});
  }

logout(){
  this.auth.logout();
  this.router.navigate(['/login']);
}

viewJob(job: IJobs){
  console.log("JOB SELECTED: " + JSON.stringify(job));
   this.jobViewService.setSelectedJob(job);
  this.router.navigate(['/job-view']);
}
}
