import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { LoginService} from '../../services/login.service';
import { LoginCompanyService} from '../../services/login-company.service';
import { AuthService} from '../../services/auth.service';
import {IUser} from  '../../interfaces/IUser';
import {ICompany} from  '../../interfaces/ICompany';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(private router: Router,private loginService: LoginService, private loginCompanyService: LoginCompanyService, private authService: AuthService) { }

  ngOnInit() {
    console.log("CURRENT USER IN LOGIN: " + sessionStorage.getItem('currentUser'));
    console.log("CURRENT TOKEN IN LOGIN: " + JSON.stringify(sessionStorage.getItem('token')));
  }

  user: IUser;
  company: ICompany;
  jsonObj: string='';
  email: string;
  password: string;
  errorMessage: string='';
  _companyCheckbox: string = 'false';
  _studentCheckbox: string = 'false';

  get _email(): string {
  return this.email;
}

set _email(value:string) {
  this.email = value;
}

get _password():string {
  return this.password;
}

set _password(value:string) {
  this.password = value;
}

  redirectToSignUp(){
    this.router.navigate(['/sign-up']);
  }

  login(email: string, password: string){
    if(this._studentCheckbox === 'true' && this._companyCheckbox === 'false') {
      console.log("First if");
      this.loginService.getLogin(this.email,this.password).subscribe(
      c=>{
        this.jsonObj = JSON.stringify(c);
        this.user=JSON.parse(this.jsonObj).user;
         console.log("USER LOGGED: "+  JSON.stringify(c));
      //   console.log("TOKEN LOGGED: " + JSON.parse(this.token).token);
        if(this.user){
          console.log("IS USER !!!");
          this.authService.login(this.user.email, this.user.firstName, this.user.lastName, JSON.parse(this.jsonObj).token);
          this.router.navigate(['/search']);
        } else {
          this.errorMessage='Invalid password or email!';
        }
        console.log('Is student');
      }
    );
  } else if ( this._companyCheckbox === 'true' && this._studentCheckbox === 'false' ) {
    console.log("Second if");
    this.loginCompanyService.getLogin(this.email,this.password).subscribe(
    c=>{
      this.jsonObj = JSON.stringify(c);
      this.company=JSON.parse(this.jsonObj).company;
      // console.log("USER: "+  JSON.stringify(c));
      // console.log("TOKEN: " + JSON.parse(this.token).token);
      if(this.company){
        this.authService.login(this.company.email, '', this.company.name, JSON.parse(this.jsonObj).token);
        this.router.navigate(['/company-home']);
      } else {
        this.errorMessage='Invalid password or email!';
      }
      console.log('Is student');
    }
  );
  }
}

studentCheckbox(event:any){
  // console.log("Student: " + JSON.stringify(event.target.checked));
  this._studentCheckbox = JSON.stringify(event.target.checked);
    console.log("Student: " +   this._studentCheckbox);
    console.log("Company: " +   this._companyCheckbox);
}

companyCheckbox(event: any){
  // console.log("Company: " +  JSON.stringify(event.target.checked));
  this._companyCheckbox = JSON.stringify(event.target.checked);
  console.log("Student: " +   this._studentCheckbox);
  console.log("Company: " + this._companyCheckbox);
}

}
