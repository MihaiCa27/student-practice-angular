import { Component, OnInit } from '@angular/core';
import { Location} from "@angular/common";
import { RouterModule, Router } from '@angular/router'

import { MyJobsService} from '../../services/my-jobs.service';
import {IJobs} from '../../interfaces/IJobs';
import {AuthService} from '../../services/auth.service';

@Component({
  selector: 'app-my-jobs',
  templateUrl: './my-jobs.component.html',
  styleUrls: ['./my-jobs.component.css']
})
export class MyJobsComponent implements OnInit {

  myJobs: IJobs[];
  errorMessage:string='';

  constructor(private myJobsService: MyJobsService, private location: Location, private router:Router, private auth: AuthService) { }

  ngOnInit() {
    this.getMyJobs();
    console.log(this.myJobs);
  }

  getMyJobs(){
  this.myJobsService.getMyJobs().subscribe(
      jobs => {
        this.myJobs = jobs;

      },
      error => this.errorMessage = <any>error
    );
  }

  removeJob(job:IJobs){
    //console.log(job._id);
    this.myJobsService.removeJob(job._id).subscribe();
     alert("Removed !");
     location.reload();
  }

  
  logout(){
    this.auth.logout();
    this.router.navigate(['/login']);
  }


}
