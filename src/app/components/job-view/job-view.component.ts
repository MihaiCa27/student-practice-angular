import { Component, OnInit } from '@angular/core';
import { RouterModule, Router } from '@angular/router';
import { HttpClient,HttpHeaders,HttpParams,HttpErrorResponse } from '@angular/common/http';
import {Observable, throwError} from 'rxjs';
import {catchError, tap, map} from 'rxjs/operators';
import { Location} from "@angular/common";

import {IJobs} from '../../interfaces/IJobs';
import {ICompany} from '../../interfaces/ICompany';
import {AuthService} from '../../services/auth.service';
import {JobService} from '../../services/job.service';
import {JobViewService} from '../../services/job-view.service';

@Component({
  selector: 'app-job-view',
  templateUrl: './job-view.component.html',
  styleUrls: ['./job-view.component.css']
})
export class JobViewComponent implements OnInit {

  constructor(private jobViewService: JobViewService, private router: Router) { }

  selectedJob:IJobs;
  jsonObj:string;
  title:string;
  description:string;
  company: ICompany;
  company_Title: string;
  category: string;
  startTime:string;
  duration:number;
  jobType:string;
  companyJson:string;
  companyTitle:string;

  ngOnInit() {
    this.selectedJob = this.jobViewService.getSelectedJob();
    this.jsonObj = JSON.stringify(this.selectedJob);
    console.log("JOB: " + this.jsonObj);
    this.title = JSON.parse(this.jsonObj).title;
    this.description= JSON.parse(this.jsonObj).description;
    this.company= JSON.parse(this.jsonObj).company;
    this.category= JSON.parse(this.jsonObj).category;
    this.startTime= JSON.parse(this.jsonObj).startTime;
    this.duration= JSON.parse(this.jsonObj).duration;
    this.jobType= JSON.parse(this.jsonObj).jobType;
    this.companyJson = JSON.stringify(this.company);
    this.companyTitle = JSON.parse(this.companyJson).name;
  }

  apply(){
    console.log("JOB ID: " + JSON.parse(this.jsonObj)._id);
    this.jobViewService.applyToJob(JSON.parse(this.jsonObj)._id).subscribe();
    alert("Succes !");
  }
}
