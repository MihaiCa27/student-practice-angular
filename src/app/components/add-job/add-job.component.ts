import { Component, OnInit } from '@angular/core';
import { RouterModule, Router } from '@angular/router';
import { HttpClient,HttpHeaders,HttpParams,HttpErrorResponse } from '@angular/common/http';
import {Observable, throwError} from 'rxjs';
import {catchError, tap, map} from 'rxjs/operators';
import { Location} from "@angular/common";

import {IJobs} from '../../interfaces/IJobs';
import {AuthService} from '../../services/auth.service';
import {AddJobService} from '../../services/add-job.service';

@Component({
  selector: 'app-add-job',
  templateUrl: './add-job.component.html',
  styleUrls: ['./add-job.component.css']
})
export class AddJobComponent implements OnInit {

  constructor(private addJobService:AddJobService) { }

  ngOnInit() {
  }

  title:string='';
  description:string='';
  category:string='';
  startTime:string='';
  duration:number=0;
  jobType:string='';

  get _title(): string {
    return this.title;
  }

  set _title(value:string) {
    this.title = value;
  }

  get _description(): string {
    return this.description;
  }

  set _description(value:string) {
    this.description = value;
  }

  get _category(): string {
    return this.category;
  }

  set _category(value:string) {
    this.category = value;
  }

  get _startTime(): string {
    return this.startTime;
  }

  set _startTime(value:string) {
    this.startTime = value;
  }

  get _duration(): number {
    return this.duration;
  }

  set _duration(value:number) {
    this.duration = value;
  }

  get _jobType(): string {
    return this.jobType;
  }

  set _jobType(value:string) {
    this.jobType = value;
  }

  addJob(){
    // console.log(this.title,this.description,this.category, this.startTime, this.duration,this.jobType);
    if(this.title !==null && this.description !==null && this.category !==null && this.startTime !==null && this.duration >= 1 && this.jobType !==null){
      console.log("All fields are complete !");
      console.log("CATEGORY: " + this.category);
      this.addJobService.addJob(this.title,this.description,this.category, this.startTime, this.duration, this.jobType).subscribe();
      alert("Added successfully !");
     location.reload();
    } else {
      alert("Fields are not complete !");
    }
  }

}
