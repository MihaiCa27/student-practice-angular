import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  jsonObj:string ='';
  token:string='';

  constructor() { }


  login(email: string, firstName:string, lastName: string, token:string ){
    // console.log("AUTH LOGIN");
   sessionStorage.setItem('currentUser', JSON.stringify({email: email, firstName:firstName, lastName:lastName}));
   sessionStorage.setItem('token', JSON.stringify({token:token}))
    console.log("CURRENT USER: " + email + " " + firstName + " " + lastName);
    console.log("TOKEN: " + token);
    // this.getToken();
 }

 logout(){
   sessionStorage.removeItem('currentUser');
   sessionStorage.removeItem('token');
 }


 getName() {
   if (sessionStorage.getItem('currentUser') == null) {
     return '';
   }
   return JSON.parse(sessionStorage.getItem('currentUser'))['name'];
 }

 getEmail() {
   if (sessionStorage.getItem('currentUser') == null) {
     return '';
   }
   return JSON.parse(sessionStorage.getItem('currentUser'))['email'];
 }



 public getRole(): string {
   if (sessionStorage.getItem('currentUser') == null) {
     return '';
   }
   return JSON.parse(sessionStorage.getItem('currentUser'))['role'];
 }

 public getToken(): string {
  //  console.log("GET TOKEN CALLED: " + JSON.parse(sessionStorage.getItem('token')).token);
  this.jsonObj = JSON.stringify(localStorage.getItem('token'));
  this.token = JSON.parse(this.jsonObj).token;
    return this.token;
  }


}
