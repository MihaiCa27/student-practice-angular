import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
import { AuthService } from 'src/app/services/auth.service';
import { JobService } from 'src/app/services/job.service';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class TokenInterceptorService implements HttpInterceptor {

  // jsonObj:string = JSON.stringify(this.auth.getToken());
  // token:string = JSON.parse(this.jsonObj).token;
  token:string='';
  dummy:string='';

  constructor(public auth: AuthService) {}


  intercept(request: HttpRequest<JobService>, next: HttpHandler): Observable<HttpEvent<any>> {
      console.log("CURRENT USER IN INTERCEPTOR: " + sessionStorage.getItem('currentUser'));
      console.log("CURRENT TOKEN IN INTERCEPTOR: " + sessionStorage.getItem('token'));
      // console.log(JSON.parse(sessionStorage.getItem('token')).token)
      if(sessionStorage.getItem('currentUser') && sessionStorage.getItem('token')) {
      request = request.clone({
        setHeaders: {
          'x-auth': /*this.auth.getToken()*/ JSON.parse(sessionStorage.getItem('token')).token //'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI1YzJmNjIwYzRmYWQwNzMwN2U0NTM3YjUiLCJhY2Nlc3MiOiJhdXRoIiwiaWF0IjoxNTQ2OTQ0MjQ2fQ.Dy6s2_4xdjpJlT-Ih2UNxP8LZv07LIuxNjRS4TuhxhM'

        }
      });
      console.log("REQUEST INTERCEPTOR: " + JSON.stringify(request));
      return next.handle(request);
    }
      request = request.clone({});
      return next.handle(request);
    

  }


}
