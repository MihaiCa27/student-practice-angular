import { Injectable } from '@angular/core';
import { HttpClient,HttpHeaders,HttpParams,HttpErrorResponse } from '@angular/common/http';
import {Observable, throwError} from 'rxjs';
import {catchError, tap, map} from 'rxjs/operators';
import {IUser} from  '../interfaces/IUser';

@Injectable({
  providedIn: 'root'
})
export class SignUpService {

  constructor(private http: HttpClient) { }
//
// existsClientWithEmail(email: string): Observable<IClient>{
//    let requestUrl='server/email';
//    return this.http.get<IUser>(requestUrl+'/'+email).pipe(catchError(this.handleError));
// }
saveClient(email: string,firstName: string,lastName: string,password: string):Observable<IUser>{
  let url='server/signup';

  let body={ email: email,  password: password, firstName: firstName, lastName: lastName};
  console.log(body);
  console.log(JSON.stringify(body));
  return this.http.post<IUser>(url,body).pipe(
    tap(
      data=>console.log('User: '+JSON.stringify(data))
    ),
    catchError(this.handleError)
   );
}

private handleError(err: HttpErrorResponse) {
  // in a real world app, we may send the server to some remote logging infrastructure
  // instead of just logging it to the console
  let errorMessage = '';
  if (err.error instanceof ErrorEvent) {
    // A client-side or network error occurred. Handle it accordingly.
    errorMessage = `An error occurred: ${err.error.message}`;
  } else {
    // The backend returned an unsuccessful response code.
    // The response body may contain clues as to what went wrong,
    errorMessage = `Server returned code: ${err.status}, error message is: ${err.message}`;
  }
  console.error(errorMessage);
  return throwError(errorMessage);
}
}
