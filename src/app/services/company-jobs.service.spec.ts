import { TestBed } from '@angular/core/testing';

import { CompanyJobsService } from './company-jobs.service';

describe('CompanyJobsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CompanyJobsService = TestBed.get(CompanyJobsService);
    expect(service).toBeTruthy();
  });
});
