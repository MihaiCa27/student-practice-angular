import { TestBed } from '@angular/core/testing';

import { LoginCompanyService } from './login-company.service';

describe('LoginCompanyService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: LoginCompanyService = TestBed.get(LoginCompanyService);
    expect(service).toBeTruthy();
  });
});
