import { Injectable } from '@angular/core';
import {HttpClient, HttpErrorResponse} from '@angular/common/http';
import {Observable, throwError} from 'rxjs';
import {catchError, tap, map} from 'rxjs/operators';
import {IJobs} from '../interfaces/IJobs';

@Injectable({
  providedIn: 'root'
})
export class CompanyJobsService {

  private userUrl = 'server/companyJobs';


constructor(private http: HttpClient) {}

withdrawJob(jobId: string) {
  let requestUrl='server/withdrawJob';
  var requestBody={jobId: jobId};
  return this.http.post<IJobs>(requestUrl,requestBody).pipe(
    tap(
      data=>{
        console.log('Job withdraw: '+JSON.stringify(data))

      }
    ),
    catchError(this.handleError)
  );
}

getJobs(): Observable<IJobs[]> {
  return this.http.get<IJobs[]>(this.userUrl).pipe(
    tap(data => console.log('All company jobs: ' + JSON.stringify(data))),
    catchError(this.handleError)
  );
}

private handleError(err: HttpErrorResponse) {
  // in a real world app, we may send the server to some remote logging infrastructure
  // instead of just logging it to the console
  let errorMessage = '';
  if (err.error instanceof ErrorEvent) {
    // A client-side or network error occurred. Handle it accordingly.
    errorMessage = `An error occurred: ${err.error.message}`;
  } else {
    // The backend returned an unsuccessful response code.
    // The response body may contain clues as to what went wrong,
    errorMessage = `Server returned code: ${err.status}, error message is: ${err.message}`;
  }
  console.error(errorMessage);
  return throwError(errorMessage);
}
}
