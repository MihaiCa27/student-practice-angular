  import { Injectable } from '@angular/core';
  import { HttpClient,HttpHeaders,HttpParams,HttpErrorResponse } from '@angular/common/http';
  import {IUser} from '../interfaces/IUser';
  import {Observable, throwError} from 'rxjs';
  import {catchError, tap, map} from 'rxjs/operators';

  @Injectable({
    providedIn: 'root'
  })
  export class LoginService {
    jsonObj: string = '';
    user:IUser;

    constructor(private http: HttpClient) { }

    getLogin(email: string, password: string): Observable<IUser>{
     let requestUrl='server/login';
     var requestBody={email: email, password: password};
     return this.http.post<IUser>(requestUrl,requestBody).pipe(
       tap(
         data=>{
           console.log('User: '+JSON.stringify(data))
          
         }
       ),
       catchError(this.handleError)
     );


  }

  private handleError(err: HttpErrorResponse) {
    // in a real world app, we may send the server to some remote logging infrastructure
    // instead of just logging it to the console
    let errorMessage = '';
    if (err.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      errorMessage = `An error occurred: ${err.error.message}`;
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      errorMessage = `Server returned code: ${err.status}, error message is: ${err.message}`;
    }
    console.error(errorMessage);
    return throwError(errorMessage);
  }
  }
