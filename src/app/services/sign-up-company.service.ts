import { Injectable } from '@angular/core';
import { HttpClient,HttpHeaders,HttpParams,HttpErrorResponse } from '@angular/common/http';
import {Observable, throwError} from 'rxjs';
import {catchError, tap, map} from 'rxjs/operators';
import {ICompany} from  '../interfaces/ICompany';
import {IUser} from  '../interfaces/IUser';

import { SignUpService } from 'src/app/services/sign-up.service';

@Injectable({
  providedIn: 'root'
})
export class SignUpCompanyService {

  constructor(private http: HttpClient, private signUpService:SignUpService) { }
//
// existsClientWithEmail(email: string): Observable<IClient>{
//    let requestUrl='server/email';
//    return this.http.get<IUser>(requestUrl+'/'+email).pipe(catchError(this.handleError));
// }
saveClient(name: string,description: string,location: string,phoneNumber: string, email:string, password:string, noOfEmployees:number):Observable<ICompany>{
  let url='server/companySignup';

  let body={ email: email, password: password, name:name, phoneNumber:phoneNumber, location:location, description:description, noOfEmployees:noOfEmployees};
  return this.http.post<ICompany>(url,body).pipe(
    tap(
      data=>console.log('Company: '+JSON.stringify(data))
    ),
    catchError(this.handleError));

    this.signUpService.saveClient(email, "", name, password);
}

private handleError(err: HttpErrorResponse) {
  // in a real world app, we may send the server to some remote logging infrastructure
  // instead of just logging it to the console
  let errorMessage = '';
  if (err.error instanceof ErrorEvent) {
    // A client-side or network error occurred. Handle it accordingly.
    errorMessage = `An error occurred: ${err.error.message}`;
  } else {
    // The backend returned an unsuccessful response code.
    // The response body may contain clues as to what went wrong,
    errorMessage = `Server returned code: ${err.status}, error message is: ${err.message}`;
  }
  console.error(errorMessage);
  return throwError(errorMessage);
}
}
