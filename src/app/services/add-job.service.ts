import { Injectable } from '@angular/core';
import { HttpClient,HttpHeaders,HttpParams,HttpErrorResponse } from '@angular/common/http';
import {IJobs} from '../interfaces/IJobs';
import {Observable, throwError} from 'rxjs';
import {catchError, tap, map} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AddJobService {

    constructor(private http: HttpClient) { }

  addJob(title: string, description:string, category:string, startTime:string, duration:number, jobType:string): Observable<IJobs>{
   let requestUrl='server/jobs';
   var requestBody={title: title, description: description, cateogry: category, startTime:startTime, duration:duration, jobType:jobType};
   console.log("Request body add job: " + JSON.stringify(requestBody));
   return this.http.post<IJobs>(requestUrl,requestBody).pipe(
     tap(
       data=>{
         console.log('Job added: '+JSON.stringify(data))

       }
     ),
     catchError(this.handleError)
   );


}

private handleError(err: HttpErrorResponse) {
  // in a real world app, we may send the server to some remote logging infrastructure
  // instead of just logging it to the console
  let errorMessage = '';
  if (err.error instanceof ErrorEvent) {
    // A client-side or network error occurred. Handle it accordingly.
    errorMessage = `An error occurred: ${err.error.message}`;
  } else {
    // The backend returned an unsuccessful response code.
    // The response body may contain clues as to what went wrong,
    errorMessage = `Server returned code: ${err.status}, error message is: ${err.message}`;
  }
  console.error(errorMessage);
  return throwError(errorMessage);
}

}
