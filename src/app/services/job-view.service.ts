import { Component, OnInit } from '@angular/core';
import { RouterModule, Router } from '@angular/router';
import { HttpClient,HttpHeaders,HttpParams,HttpErrorResponse } from '@angular/common/http';
import {Observable, throwError} from 'rxjs';
import {catchError, tap, map} from 'rxjs/operators';
import { Location} from "@angular/common";
import { Injectable } from '@angular/core';


import {IJobs} from '../interfaces/IJobs';
import {AuthService} from './auth.service';
import {JobService} from './job.service';


@Injectable({
  providedIn: 'root'
})
export class JobViewService {

  selectedJob:IJobs;

    constructor(private http: HttpClient) { }

  setSelectedJob(job:IJobs){
    this.selectedJob = job;
  }

  getSelectedJob():IJobs {
    return this.selectedJob;
  }

  applyToJob(jobId: string): Observable<IJobs>{
   let requestUrl='server/applyToJob';
   var requestBody={jobId: jobId};
   return this.http.post<IJobs>(requestUrl,requestBody).pipe(
     tap(
       data=>{
         console.log('User: '+JSON.stringify(data))

       }
     ),
     catchError(this.handleError)
   );
  }


  private handleError(err: HttpErrorResponse) {
  // in a real world app, we may send the server to some remote logging infrastructure
  // instead of just logging it to the console
  let errorMessage = '';
  if (err.error instanceof ErrorEvent) {
    // A client-side or network error occurred. Handle it accordingly.
    errorMessage = `An error occurred: ${err.error.message}`;
  } else {
    // The backend returned an unsuccessful response code.
    // The response body may contain clues as to what went wrong,
    errorMessage = `Server returned code: ${err.status}, error message is: ${err.message}`;
  }
  console.error(errorMessage);
  return throwError(errorMessage);
  }

}
