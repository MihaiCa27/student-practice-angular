import { TestBed } from '@angular/core/testing';

import { SignUpCompanyService } from './sign-up-company.service';

describe('SignUpCompanyService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SignUpCompanyService = TestBed.get(SignUpCompanyService);
    expect(service).toBeTruthy();
  });
});
