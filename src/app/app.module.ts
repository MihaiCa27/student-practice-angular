import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { HTTP_INTERCEPTORS } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './components/login/login.component';
import { SearchComponent } from './components/search/search.component';
import { SignUpComponent } from './components/sign-up/sign-up.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { CompanyComponent } from './components/company/company.component';
import {LoginService} from './services/login.service';
import { SignUpCompanyComponent } from './components/sign-up-company/sign-up-company.component';
import { TokenInterceptorService } from './services/token-interceptor.service';
import { JobViewComponent } from './components/job-view/job-view.component';
import { MyJobsComponent } from './components/my-jobs/my-jobs.component';
import { AddJobComponent } from './components/add-job/add-job.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    SearchComponent,
    SignUpComponent,
    NavbarComponent,
    CompanyComponent,
    SignUpCompanyComponent,
    JobViewComponent,
    MyJobsComponent,
    AddJobComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [LoginService,
    {
     provide: HTTP_INTERCEPTORS,
     useClass: TokenInterceptorService,
     multi: true
   }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
