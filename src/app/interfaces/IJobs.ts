import {ICompany} from './ICompany';
import {IUser} from './IUser';

export class IJobs {
  _id : string;
  title: string;
  description: string;
  company : ICompany;
  category : string;
  startTime : string;
  duration : number;
  jobType: string;
  applicants : string[];
}
