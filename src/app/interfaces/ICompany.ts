export class ICompany {
  id:string;
  name:string;
  description:string;
  location:string;
  phoneNumber:string;
  email:string;
  noOfEmployees:number;
}
